#ifndef _petsc_eig_h
#define _petsc_eig_h

#include <petscksp.h>

typedef struct _p_PetscEig *PetscEig;

PETSC_EXTERN PetscErrorCode PetscEigCreate(MPI_Comm,PetscEig*);
PETSC_EXTERN PetscErrorCode PetscEigSetFromOptions(PetscEig);
PETSC_EXTERN PetscErrorCode PetscEigAnalyze(PetscEig);
PETSC_EXTERN PetscErrorCode PetscEigDestroy(PetscEig*);

#endif /* _petsc_eig_h */
