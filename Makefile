# -*- mode: makefile -*-

include ${SLEPC_DIR}/conf/slepc_variables

OBJDIR := $(PETSC_ARCH)/obj
LIBDIR := $(PETSC_ARCH)/lib

all : $(LIBDIR)/libpetsc-eig.so

.SECONDEXPANSION:		# to expand $$(@D)/.DIR

ifeq ($(V),)
  quiet_HELP := "Use \"$(MAKE) V=1\" to see the verbose compile lines.\n"
  quiet = @printf $(quiet_HELP)$(eval quiet_HELP:=)"  $1 $@\n"; $($1)
else ifeq ($(V),0)		# Same, but do not print any help
  quiet = @printf "  $1 $@\n"; $($1)
else				# Show the full command line
  quiet = $($1)
endif

PETSCEIG_CCPPFLAGS := $(CCPPFLAGS) -Iinclude
PETSC_COMPILE.c = $(call quiet,CC) -c $(PCC_FLAGS) $(CFLAGS) $(PETSCEIG_CCPPFLAGS) -MMD -MP

srcs := $(shell find src -name *.c)
srcs.o = $(srcs:%.c=$(OBJDIR)/%.o)
srcs.d = $(srcs.o:%.o=%.d)

$(LIBDIR)/libpetsc-eig.so : $(srcs.o) | $$(@D)/.DIR
	$(call quiet,CLINKER) -shared -o $@ $^ $(SLEPC_LIB)

$(OBJDIR)/%.o : %.c | $$(@D)/.DIR
	$(PETSC_COMPILE.c) $< -o $@

%/.DIR :
	@mkdir -p $(@D)
	@touch $@

.PRECIOUS: %/.DIR

.PHONY: clean

clean:
	rm -rf $(OBJDIR) $(LIBDIR)/libpetsc*

-include $(srcs.d)
