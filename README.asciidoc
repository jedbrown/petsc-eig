PETSc-Eig
=========

PETSc-Eig is a plugin to unintrusively perform eigen-analysis of linear
systems appearing in PETSc solvers.

== Installation

The plugin depends on SLEPc, but applications need not know about SLEPc.
To install, set `PETSC_DIR`, `PETSC_ARCH`, and `SLEPC_DIR`, then run

    $ make

== Usage
Run your application with

    $ ./your-app -dll_append /path/to/petsc-eig/$PETSC_ARCH/lib -yourprefix_ksp_plugin eig

The eigenvectors to look for are found under `-yourprefix_ksp_eps_*`.
See the SLEPc documentation for details.

Visualization of eigenvectors is performed using `VecView()`, so the
plugin is most useful when vectors have been obtained from a PETSc `DM`
or have custom view operations set, as in:

  VecSetOperation(X,VECOP_VIEW,VecView_CustomUserImpl);
  VecSetOperation(X,VECOP_DUPLICATE,VecDuplicate_CustomUserImpl);
