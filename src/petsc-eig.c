#define PETSC_DESIRE_COMPLEX

#include <petsc-eig.h>
#include <slepceps.h>
#include <petsc-private/petscimpl.h>          /* For PetscLogObjectMemory */

typedef enum {PETSC_EIG_KSP_UNPRECONDITIONED, PETSC_EIG_KSP_PRECONDITIONED, PETSC_EIG_KSP_MGSMOOTHER, PETSC_EIG_KSP_MGCOARSE} PetscEigKSPType;
const char *const PetscEigKSPTypes[] = {"UNPRECONDITIONED","PRECONDITIONED","MGSMOOTHER","MGCOARSE","PetscEigKSPType","PETSC_EIG_KSP_",NULL};

typedef enum {PETSC_EIG_VEC_REAL,PETSC_EIG_VEC_IMAG,PETSC_EIG_VEC_MAGNITUDE,PETSC_EIG_VEC_PHASE} PetscEigVecOutputType;
const char *const PetscEigVecOutputTypes[] = {"REAL","IMAG","MAGNITUDE","PHASE","PetscEigVecOutputType","PETSC_EIG_VEC_",NULL};

typedef struct _n_PetscEigKSP *PetscEigKSP;
struct _n_PetscEigKSP {
  EPS eps;
  char *templatevtk;
  PetscInt solvenum;
  PetscEigKSPType eigtype;
  PetscEigVecOutputType otype;
  PetscViewer viewer;
  PetscViewerFormat viewformat;
  Vec workvec;

  Mat Amat;
  PC pc;
};

#undef __FUNCT__
#define __FUNCT__ "MatMult_PetscEig_Preconditioned"
static PetscErrorCode MatMult_PetscEig_Preconditioned(Mat A,Vec x,Vec y)
{
  PetscEigKSP eig;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = MatShellGetContext(A,&eig);CHKERRQ(ierr);
  if (!eig->workvec) {
    ierr = VecDuplicate(x,&eig->workvec);CHKERRQ(ierr);
  }
  ierr = MatMult(eig->Amat,x,eig->workvec);CHKERRQ(ierr);
  ierr = PCApply(eig->pc,eig->workvec,y);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PetscEigKSPSetFromOptions"
PETSC_INTERN PetscErrorCode PetscEigKSPSetFromOptions(PetscObject oksp,void *ctx)
{
  KSP ksp = (KSP)oksp;
  PetscEigKSP eig = (PetscEigKSP)ctx;
  PetscErrorCode ierr;
  char templatevtk[PETSC_MAX_PATH_LEN],templatevtkdefault[] = "eigenvectors-%03D-%03D.vts";
  PetscBool flg;

  PetscFunctionBegin;
  ierr = PetscOptionsHead("Eigen-analysis plugin");CHKERRQ(ierr);
  ierr = PetscOptionsEnum("-eig_type","Aspect of KSP on which to perform eigenanalysis","",PetscEigKSPTypes,(PetscEnum)eig->eigtype,(PetscEnum*)&eig->eigtype,NULL);CHKERRQ(ierr);
  if (!eig->eps) {
    const char *prefix;
    ierr = EPSCreate(PetscObjectComm((PetscObject)ksp),&eig->eps);CHKERRQ(ierr);
    ierr = KSPGetOptionsPrefix(ksp,&prefix);CHKERRQ(ierr);
    ierr = EPSSetOptionsPrefix(eig->eps,prefix);CHKERRQ(ierr);
    ierr = EPSAppendOptionsPrefix(eig->eps,"eig_");CHKERRQ(ierr);
  }
  ierr = EPSSetFromOptions(eig->eps);CHKERRQ(ierr);
  ierr = PetscOptionsString("-eig_view_vectors_vtk","Save each eigenvector to a VTK file","VecView",templatevtk,templatevtk,sizeof(templatevtk),&flg);CHKERRQ(ierr);
  if (flg) {
    ierr = PetscFree(eig->templatevtk);CHKERRQ(ierr);
    ierr = PetscStrallocpy(templatevtk[0]?templatevtk:templatevtkdefault,&eig->templatevtk);CHKERRQ(ierr);
  }
  ierr = PetscOptionsEnum("-eig_vec_output_type","Eigenvector output representation","",PetscEigVecOutputTypes,(PetscEnum)eig->otype,(PetscEnum*)&eig->otype,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetViewer(PetscObjectComm((PetscObject)oksp),((PetscObject)oksp)->prefix,"-eig_view",&eig->viewer,&eig->viewformat,&flg);CHKERRQ(ierr);
  if (!flg) {
    ierr = PetscViewerASCIIGetStdout(PetscObjectComm((PetscObject)oksp),&eig->viewer);CHKERRQ(ierr);
    ierr = PetscObjectReference((PetscObject)eig->viewer);CHKERRQ(ierr); /* So that we can destroy it later */
    eig->viewformat = PETSC_VIEWER_DEFAULT;
  }
  ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PetscEigKSPDestroy"
PETSC_INTERN PetscErrorCode PetscEigKSPDestroy(PetscObject oksp,void *ctx)
{
  PetscEigKSP eig = (PetscEigKSP)ctx;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = EPSDestroy(&eig->eps);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&eig->viewer);CHKERRQ(ierr);
  ierr = VecDestroy(&eig->workvec);CHKERRQ(ierr);
  ierr = MatDestroy(&eig->Amat);CHKERRQ(ierr);
  ierr = PCDestroy(&eig->pc);CHKERRQ(ierr);
  ierr = PetscFree(eig->templatevtk);CHKERRQ(ierr);
  ierr = PetscFree(eig);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PetscEigKSPSetUp"
static PetscErrorCode PetscEigKSPSetUp(KSP ksp,PetscEigKSP eig)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  switch (eig->eigtype) {
  case PETSC_EIG_KSP_UNPRECONDITIONED: {
    ST st;
    KSP stksp;
    Mat Amat;
    ierr = EPSSetProblemType(eig->eps,EPS_NHEP);CHKERRQ(ierr);
    ierr = KSPGetOperators(ksp,&Amat,NULL,NULL);CHKERRQ(ierr);
    ierr = EPSSetOperators(eig->eps,Amat,NULL);CHKERRQ(ierr);
    ierr = EPSGetST(eig->eps,&st);CHKERRQ(ierr);
    ierr = STGetKSP(st,&stksp);CHKERRQ(ierr);
    if (stksp) {
      PC pc;
      ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
      ierr = KSPSetPC(stksp,pc);CHKERRQ(ierr);
    }
  } break;
  case PETSC_EIG_KSP_PRECONDITIONED: {
    Mat mshell;
    PetscInt m,n,M,N;
    ST st;
    KSP stksp;
    PC stpc;
    ierr = EPSSetProblemType(eig->eps,EPS_NHEP);CHKERRQ(ierr);
    ierr = EPSGetST(eig->eps,&st);CHKERRQ(ierr);
    ierr = STGetKSP(st,&stksp);CHKERRQ(ierr);
    ierr = KSPSetType(stksp,KSPGMRES);CHKERRQ(ierr);
    ierr = KSPGetPC(stksp,&stpc);CHKERRQ(ierr);
    ierr = PCSetType(stpc,PCNONE);CHKERRQ(ierr);

    ierr = MatDestroy(&eig->Amat);CHKERRQ(ierr);
    ierr = KSPGetOperators(ksp,&eig->Amat,NULL,NULL);CHKERRQ(ierr);
    ierr = PetscObjectReference((PetscObject)eig->Amat);CHKERRQ(ierr);
    ierr = PCDestroy(&eig->pc);CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&eig->pc);CHKERRQ(ierr);
    ierr = PetscObjectReference((PetscObject)eig->pc);CHKERRQ(ierr);

    ierr = MatGetSize(eig->Amat,&M,&N);CHKERRQ(ierr);
    ierr = MatGetLocalSize(eig->Amat,&m,&n);CHKERRQ(ierr);
    ierr = MatCreateShell(PetscObjectComm((PetscObject)ksp),m,n,M,N,eig,&mshell);CHKERRQ(ierr);
    ierr = MatShellSetOperation(mshell,MATOP_MULT,(void(*)(void))MatMult_PetscEig_Preconditioned);CHKERRQ(ierr);
    ierr = EPSSetOperators(eig->eps,mshell,NULL);CHKERRQ(ierr);
    ierr = MatDestroy(&mshell);CHKERRQ(ierr);
  } break;
  default:
    SETERRQ1(PetscObjectComm((PetscObject)ksp),PETSC_ERR_SUP,"No support for %s",PetscEigKSPTypes[eig->eigtype]);
  }
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "PetscEigKSPPostSolve"
PETSC_INTERN PetscErrorCode PetscEigKSPPostSolve(KSP ksp,Vec b,Vec x,void *ctx)
{
  PetscEigKSP eig = (PetscEigKSP)ctx;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscEigKSPSetUp(ksp,eig);CHKERRQ(ierr);
  {
    EPSType type;
    PetscInt nev,its,i;
    ierr = EPSSolve(eig->eps);CHKERRQ(ierr);
    ierr = EPSGetType(eig->eps,&type);CHKERRQ(ierr);
    ierr = PetscViewerPushFormat(eig->viewer,eig->viewformat);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(eig->viewer," Solution method: %s\n",type);CHKERRQ(ierr);
    ierr = EPSGetDimensions(eig->eps,&nev,NULL,NULL);CHKERRQ(ierr);
    ierr = EPSGetIterationNumber(eig->eps,&its);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(eig->viewer," Number of requested eigenvalues: %D  iterations: %D\n",nev,its);CHKERRQ(ierr);
    for (i=0; i<nev; i++) {
      PetscScalar eigr,eigi;
      PetscComplex ew;
      PetscReal errest;
      ierr = EPSGetEigenvalue(eig->eps,i,&eigr,&eigi);CHKERRQ(ierr);
      ew = eigr + PETSC_i * eigi;
      ierr = EPSGetErrorEstimate(eig->eps,i,&errest);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPrintf(eig->viewer,"  Eigenvalue % 2D (error): %G+%Gi (%G)\n",i,PetscRealPartComplex(ew),PetscImaginaryPartComplex(ew),errest);CHKERRQ(ierr);
    }
    if (eig->templatevtk) {
      Vec sol,Vr,Vi;
      ierr = KSPGetSolution(ksp,&sol);CHKERRQ(ierr);
      ierr = VecDuplicate(sol,&Vr);CHKERRQ(ierr);
      ierr = VecDuplicate(sol,&Vi);CHKERRQ(ierr);
      for (i=0; i<nev; i++) {
        char filepath[PETSC_MAX_PATH_LEN];
        PetscViewer vtk;
        ierr = PetscSNPrintf(filepath,sizeof filepath,eig->templatevtk,eig->solvenum,i);CHKERRQ(ierr);
        ierr = PetscViewerVTKOpen(PetscObjectComm((PetscObject)ksp),filepath,FILE_MODE_WRITE,&vtk);CHKERRQ(ierr);
        ierr = EPSGetEigenvector(eig->eps,i,Vr,Vi);CHKERRQ(ierr);
        switch (eig->otype) {
        case PETSC_EIG_VEC_REAL:
          ierr = VecView(Vr,vtk);CHKERRQ(ierr);
          break;
        default:
          SETERRQ1(PetscObjectComm((PetscObject)ksp),PETSC_ERR_SUP,"%s not implemented",PetscEigVecOutputTypes[eig->otype]);
        }
        ierr = PetscViewerDestroy(&vtk);CHKERRQ(ierr);
      }
      ierr = VecDestroy(&Vr);CHKERRQ(ierr);
      ierr = VecDestroy(&Vi);CHKERRQ(ierr);
    }
    ierr = PetscViewerPopFormat(eig->viewer);CHKERRQ(ierr);
  }
  eig->solvenum++;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "KSPActivatePlugin_Eig"
PETSC_INTERN PetscErrorCode KSPActivatePlugin_Eig(KSP ksp)
{
  PetscErrorCode ierr;
  PetscEigKSP eig;
  PetscBool flg;

  PetscFunctionBegin;
  ierr = SlepcInitialized(&flg);CHKERRQ(ierr);
  if (!flg) {
    ierr = SlepcInitialize(NULL,NULL,NULL,"PetscEig plugin\n");CHKERRQ(ierr);
    ierr = PetscRegisterFinalize(SlepcFinalize);CHKERRQ(ierr);
  }
  ierr = PetscNewLog(ksp,struct _n_PetscEigKSP,&eig);CHKERRQ(ierr);
  eig->eigtype = PETSC_EIG_KSP_PRECONDITIONED;
  eig->otype = PETSC_EIG_VEC_REAL;
  ierr = PetscObjectAddOptionsHandler((PetscObject)ksp,PetscEigKSPSetFromOptions,PetscEigKSPDestroy,eig);CHKERRQ(ierr);
  ierr = KSPSetPostSolve(ksp,PetscEigKSPPostSolve,eig);CHKERRQ(ierr); /* Bad because this would conflict with other uses of post-solve */
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PetscDLLibraryRegister_petsc_eig"
PETSC_EXTERN PetscErrorCode PetscDLLibraryRegister_petsc_eig(char *path)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  /* This is usually called inside PetscInitialize, prior to PetscInitializeCalled being set. */
  ierr = KSPPluginRegister("eig",KSPActivatePlugin_Eig);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
